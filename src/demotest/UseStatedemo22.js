import react, { useState,useEffect } from 'react'
import Table from 'react-bootstrap/Table'
const axios = require('axios');
export default function UseStatedemo22() {
    // let yaer=[];
    // for(let i = 1990;i<2020;i++){
    //     yaer.push(i)
    // }
    
    const[newData,setNewData]=useState()
    useEffect(()=>  {
        fetch('https://api.covid19api.com/summary')
        .then(response => response.json())
        .then(data => setNewData(data));
    },[])

    console.log('object======>>>',newData?.Countries)
    return (
        <div style={{padding:40}}>
           <Table striped bordered hover>
  <thead>
    <tr>
      <th>No.</th>
      <th>Country</th>
      <th>Date</th>
      <th>Username</th>
    </tr>
  </thead>
  <tbody>
      {newData?.Countries?.map((data,index)=>{
          return(
            <tr>
            <td>{index +1}</td>
            <td>{data?.Country}</td>
            <td>{data?.Date}</td>
            <td>@mdo</td>
          </tr>
          )
      })}
 
  </tbody>
</Table>
        </div>
    );
}