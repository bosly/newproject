import react,{useState} from 'react';


function UseStatearray(){
    const intialState = {username: "", password: "" }
    const [account, setAccount] = useState (intialState)
    const [accountlist, setAccountList] = useState([])
    return(
        <div>
            {/* <p>#Debug {JSON.stringify(account)}</p> */}
            <p>#Debug {JSON.stringify(accountlist)}</p>
            <form>
                <input type="text" placeholder="Username" 
                    value={account.username}
                    onChange={e=>{
                    setAccount({...account, username: e.target.value, })
                }} />
                <br/>
                <input type="text" placeholder="Password" 
                    value={account.password}
                    onChange={e=>{
                    setAccount({...account, password: e.target.value })
                }}/>
                <br/>
                <button
                onClick={e => {
                    e.preventDefault()
                    setAccountList([ ...accountlist, {...account, index: accountlist.length} ])
                }}
                >
                Submit
                </button>
                <button
                type="button"
                onClick={e => {
                e.preventDefault();
                setAccount(intialState)
                }}
                >Clear</button>
            </form>
            <ul>
                {accountlist.map(item=><li key={item.index}>username: {item.username}, password: {item.password}</li>)}
            </ul>
        </div>
    );
}
export default UseStatearray;