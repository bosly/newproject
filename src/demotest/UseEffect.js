import react,{useState,useEffect} from 'react';

function UseEffect(){
    const [count, setCount] = useState(0);

    useEffect(()=>  {
        document.title ="Current Count: " + count
    },[count])
    return(
        <div>
            <h1>{count}</h1>
            <button onClick={()=> setCount(count + 1)}>ADD</button>
        </div>
    );
}

export default UseEffect;