import './App.css';
import React, { useState } from 'react';
import Statedemo1 from "./demotest/Statedemo1";
import Statedemo2 from "./demotest/Statedemo2";
import UseStatearray from "./demotest/UseStatearray";
import UseEffect from "./demotest/UseEffect";
import UseStatedemo22 from "./demotest/UseStatedemo22";
import Home from "./demotest/Home";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';



function App() {
  return (
    <div className="App">
      <Router>
        
          <Home/>
          <Switch>
          <Route path="/" exact component={Hous}/>
          <Route path="/statedemo1" component={Statedemo1}/>
          <Route path="/useStatedemo22" component={UseStatedemo22}/>
          <Route path="/home" component={Home}/>
        </Switch>
      </Router>
    </div>
  );
}
const Hous = () =>(
  <div>
    <h3>Home</h3>
  </div>
);

export default App;
