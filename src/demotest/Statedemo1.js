import React, { useState } from 'react';

function Statedemo1() {
  const [cont, setCont] = useState (0)
  const [text, setText] = useState ("")
  return (
    <div style={{ textAlign : "center", top:100}}>
      <button onClick={()=> {
        setCont(cont + 2);
        setText("Bo souliy")
      
      }}>Add</button>
      Usesatae : {cont}
      <br/>
      Text : {text} {cont}
    </div>
  );
}

export default Statedemo1;